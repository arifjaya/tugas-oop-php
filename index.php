<?php
require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';

$sheep = new Animal("shaun");
echo "Nama : ";
echo $sheep->name; // "shaun"
echo "<br>";
echo "legs : ";
echo $sheep->legs; // 4
echo "<br>";
echo "cold blooded : ";
echo $sheep->cold_blooded; // "no"
echo "<br>";
echo "<br>";
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

$kodok = new Frog("buduk");
echo "Nama : ";
echo $kodok->name; // "shaun"
echo "<br>";
echo "legs : ";
echo $kodok->legs; // 4
echo "<br>";
echo "cold blooded : ";
echo $kodok->cold_blooded; // "Yes"
echo "<br>";
echo "Jump : ";
echo $kodok->jump() ; // "hop hop"
echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Nama : ";
echo $sungokong->name; // "shaun"
echo "<br>";
echo "legs : ";
echo $sungokong->legs; // 2
echo "<br>";
echo "cold blooded : ";
echo $sungokong->cold_blooded; // "no"
echo "<br>";
echo "Yell : ";
$sungokong->yell() // "Auooo"


?>